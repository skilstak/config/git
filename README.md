# SkilStak Git, GitLab, GitHub Configuration

*[This repo is mirrored from ![Fox](gitlab.png) GitLab (a far superior open-source option).](https://gitlab.com/skilstak/config/git)*


![Large Git Logo](large-logo.png)

![A GitLab-Centric Save](gitlab-beats-github-save.png)

GitLab is objectively the best hosting service for all `git` projects. This configuration assumes the following:

* GitLab is main hosting provider.
* GitHub is used for mirroring all public repos.
* Groups and subgroups translate to dashes on GitHub.
* Only `ssh` used (no `http` other than read-only pulls).
